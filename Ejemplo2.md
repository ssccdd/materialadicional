# Organización de atributos independientes en clases sincronizadas

La palabra reservada `synchronized` no sólo puede aplicarse a los métodos. Otra aplicación es a un bloque de código y con la siguiente sintaxis:

```java
synchronized (object) {

    // código Java

}
```

De esta forma se establece una **sección crítica** de código Java cuyo acceso está controlado por la referencia a un objeto. Una forma es mediante la *autorreferencia* `this` del objeto que está ejecutando el método. Pero se puede utilizar la referencia a otro objeto creado específicamente para garantizar el acceso a la **sección crítica**. El poder **sincronizar** un bloque de código y no sólo un método nos permite el acceso concurrente de atributos independientes de un objeto. De esta forma se incrementa el rendimiento de las aplicaciones concurrentes.

Para dejar más claro este concepto, vamos a simular el acceso a dos salas de cine y de dos ventanillas que gestionan los asientos disponibles para cada una de las salas. Las entradas a las salas se venden separadamente, por tanto el número de asientos disponibles en cada sala es independiente entre sí.

1. Definimos una clase llamada `Cinema` y creamos dos variables de instancia que nos llevará el número de asientos disponibles en cada sala. Además necesitaremos dos objetos que nos controlarán el acceso a la sección crítica cuando gestionen los asientos de cada sala. El constructor dará un valor para cada atributo de la clase.

```java
...
public class Cinema {
	
    /**
     * This two variables store the vacancies in two cinemas
     */
    private long vacanciesCinema1;
    private long vacanciesCinema2;

    /**
     * Two objects for the synchronization. ControlCinema1 synchronizes the
     * access to the vacancesCinema1 attribute and controlCinema2 synchronizes
     * the access to the vacanciesCinema2 attribute.
     */
    private final Object controlCinema1, controlCinema2;
	
    /**
     * Constructor of the class. Initializes the objects
     */
    public Cinema(){
        controlCinema1=new Object();
        controlCinema2=new Object();
        vacanciesCinema1=20;
        vacanciesCinema2=20;
    }
...
```

2. Implementamos el método que nos permite vender entradas para la sala 1, `sellTickets1(.)`. Utilizaremos el objeto que controla la gestión de las entradas para la sala 1 para definir la **sección crítica**.

```java
...
/**
 * This method implements the operation of sell tickets for the cinema 1
 * @param number number of tickets sold
 * @return true if the tickets are sold, false if there is no vacancies
 */
public boolean sellTickets1 (int number) {
    synchronized (controlCinema1) {
        if (number<vacanciesCinema1) {
            vacanciesCinema1-=number;
            return true;
        } else {
            return false;
        }
    }
}
...
```

3. También deberemos implementar un método para gestionar la salida de un cliente de la sala 1 y así dejar un asiento disponible para la sala 1. Volveremos a necesitar el mismo objeto que en el método anterior para garantizar la consistencia de los asientos disponibles para la sala 1.

```java
...
/**
 * This method implements the operation of return tickets for the cinema 1
 * @param number number of the tickets returned
 * @return true
 */
public boolean returnTickets1 (int number) {
    synchronized (controlCinema1) {
        vacanciesCinema1+=number;
        return true;
    }
}
...
```

4. Para la sala 2 implementaremos dos métodos similares a los anteriores, pero utilizaremos el objeto que controlará la sección crítica de la sala 2. De esta forma se garantiza la máxima concurrencia ya que los datos de una sala no afectan al funcionamiento de la otra sala y pueden ejecutarse concurrentemente los métodos asociados.

5. Por último implementaremos un método, uno para cada sala, que nos devuelva el número de asientos libres.

```java
...
/**
 * Return the vacancies in the cinema 1
 * @return the vacancies in the cinema 1
 */
public long getVacanciesCinema1() {
    return vacanciesCinema1;
}
...
```

6. Para representar las ventanillas de venta de entradas definimos una clase para cada una de ellas. Mostraré el caso para la ventanilla 1; definimos una clase de nombre `TicketOffice1` que implementa la interface `Runnable`. Creamos una variable de instancia que representa el cine. Además crearemos su constructor para asociar el cine a la ventanilla de venta.

```java
...
/**
 * This class simulates a ticket office. It sell or return tickets
 * for the two cinemas
 *
 */
public class TicketOffice1 implements Runnable {

    /**
     * The cinema 
     */
    private Cinema cinema;
	
    /**
     * Constructor of the class
     * @param cinema the cinema
     */
    public TicketOffice1 (Cinema cinema) {
        this.cinema=cinema;
    }
...
```

7. El método `run(.)` representará la actividad de venta y recogida de entradas de las salas de cine disponibles. En nuestro caso simularemos la venta para cada una de las salas así como la salida (retorno de las entradas) de los clientes.

```java
...
/**
 * Core method of this ticket office. Simulates selling and returning tickets
 */
@Override
public void run() {
    cinema.sellTickets1(3);
    cinema.sellTickets1(2);
    cinema.sellTickets2(2);
    cinema.returnTickets1(3);
    cinema.sellTickets1(5);
    cinema.sellTickets2(2);
    cinema.sellTickets2(2);
    cinema.sellTickets2(2);
}
...
```

8. La segunda ventanilla tendrá una codificación similar; **ejercicio**: implementar una simulación diferente en el método `run(.)`.

9. Implementaremos el método `main(.)` de la aplicación Java. Creamos un objeto para la clase `Cinema` y objetos para cada una de las clases que representan las ventanillas. Crearemos objetos de la clase `Thread` y le asociaremos la ventanilla correspondiente antes de iniciar su ejecución.

```java
...
// Creates a Cinema
Cinema cinema=new Cinema();
		
// Creates a TicketOffice1 and a Thread to run it
TicketOffice1 ticketOffice1=new TicketOffice1(cinema);
Thread thread1=new Thread(ticketOffice1,"TicketOffice1");

// Creates a TicketOffice2 and a Thread to run it
TicketOffice2 ticketOffice2=new TicketOffice2(cinema);
Thread thread2=new Thread(ticketOffice2,"TicketOffice2");
		
// Starts the threads
thread1.start();
thread2.start();
...
```

10. Esperaremos a la finalización de los hilos asociados a las ventanillas antes de presentar por la consola el número de asientos disponibles.

```java
...
try {
    // Waits for the finalization of the threads
    thread1.join();
    thread2.join();
} catch (InterruptedException e) {
    e.printStackTrace();
}

// Print the vacancies in the cinemas
System.out.printf("Room 1 Vacancies: %d\n",cinema.getVacanciesCinema1());
System.out.printf("Room 2 Vacancies: %d\n",cinema.getVacanciesCinema2());
...
```

## Ejercicios propuestos

 
- Modificar el ejercicio para que la venta y la salida del cine sea controlados por diferentes hilos.
- Ver la posibilidad para unificar en un único método la venta y devolución de las entradas para gestionar las dos salas y que se mantenga el mismo nivel de concurrencia. 
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTU1MzE4MTQ3MCw2NjYxNDUzOTJdfQ==
-->