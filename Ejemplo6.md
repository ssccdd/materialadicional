# Intercambiando información entre tareas concurrentes

Java proporciona una utilidad de sincronización para el intercambio de información entre dos tareas concurrentes. La clase [`Exchanger`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Exchanger.html) permite definir un **punto de sincronización** entre dos tareas. Cuando las dos tareas alcanzan ese punto, ellas intercambian una estructura de datos, esto es, la estructura de datos del primer hilo pasa al segundo y la estructura de datos del segundo hilo pasa al primero. La clase Exchanger puede ser muy útil en situaciones similares al problema del **Productor/Consumidor**. Este es un problema clásico de la concurrencia donde se dispone de un buffer  común de datos, uno o más productores añaden datos, y uno o más consumidores retiran datos. Como la clase `Exchanger` sólo puede sincronizar dos hilos, se podrá utilizar en el problema del **Productor/Consumidor** con sólo un productor y sólo un consumidor, no para un caso más general.

En el ejemplo se mostrará cómo utilizar la clase `Exchanger` para el problema del **Productor/Consumidor** con sólo un productor y sólo un consumidor.

1. Primero definiremos el productor mediante la clase `Producer` que implementa la interface `Runnable`.

2. Como variables de instancia tendremos:
	- Un `buffer` que representará la estructura de datos que se intercambia.
	- Una referencia de la clase `Exchanger` para el intercambio de la estructura de datos. Deberá ser del mismo tipo que la estructura que se desea intercambiar.
    
3. El constructor inicializa estas dos variables de instancia con los valores apropiados.

```java
...
/**
 * Buffer to save the events produced
 */
private List<String> buffer;
	
/**
 * Exchager to synchronize with the consumer
 */
private final Exchanger<List<String>> exchanger;
	
/**
 * Constructor of the class. Initializes its attributes
 * @param buffer Buffer to save the events produced
 * @param exchanger Exchanger to syncrhonize with the consumer
 */
public Producer (List<String> buffer, Exchanger<List<String>> exchanger){
    this.buffer=buffer;
    this.exchanger=exchanger;
}
...
```

4. Implementamos el método `run()` donde se producirán los datos. Realizaremos 10 ciclos de producción. En cada uno de ellos se producirán 10 datos que se almacenarán el `buffer`. Una vez producidos los 10 elementos los intercambiamos con el consumidor por un nuevo `buffer` vacío para el siguiente ciclo de producción. 

```java
...
/**
 * Main method of the producer. It produces 100 events. 10 cicles of 10 events.
 * After produce 10 events, it uses the exchanger object to synchronize with 
 * the consumer. The producer sends to the consumer the buffer with ten events and
 * receives from the consumer an empty buffer
 */
@Override
public void run() {
    int cycle=1;
		
    for (int i=0; i<10; i++){
        System.out.printf("Producer: Cycle %d\n",cycle);
			
        for (int j=0; j<10; j++){
            String message="Event "+((i*10)+j);
            System.out.printf("Producer: %s\n",message);
            buffer.add(message);
        }
			
        try {
            /*
             * Change the data buffer with the consumer
             */
             buffer=exchanger.exchange(buffer);
         } catch (InterruptedException e) {
             e.printStackTrace();
         }
			
         System.out.printf("Producer: %d\n",buffer.size());
			
         cycle++;
     }
}
...
```

5. Ahora implementamos el consumidor mediante la clase `Consumer` que implementa la interface `Runnable`.

6. Como variables de instancia tendremos:
	- Un `buffer` que representará la estructura de datos que se intercambia.
	- Una referencia de la clase `Exchanger` para el intercambio de la estructura de datos. Deberá ser del mismo tipo que la estructura que se desea intercambiar.
    
7. El código es similar la presentado en la clase `Producer` y se puede revisar en el ejemplo de la sesión.

8. Implementamos el método `run()` para realizar las operaciones relativas al consumidor. Se realizarán 10 ciclos de consumo. Para cada ciclo se intercambia con el productor el buffer de de datos por el del consumidor que se encuentra vacío. Luego se realizan las operaciones necesarias para consumir esos datos antes de empezar el siguiente ciclo.

```java
...
/**
 * Main method of the producer. It consumes all the events produced by the Producer. After
 * processes ten events, it uses the exchanger object to synchronize with 
 * the producer. It sends to the producer an empty buffer and receives a buffer with ten events
 */
@Override
public void run() {
    int cycle=1;
		
    for (int i=0; i<10; i++){
        System.out.printf("Consumer: Cycle %d\n",cycle);

        try {
            // Wait for the produced data and send the empty buffer to the producer
            buffer=exchanger.exchange(buffer);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
			
        System.out.printf("Consumer: %d\n",buffer.size());
			
        for (int j=0; j<10; j++){
            String message=buffer.get(0);
            System.out.printf("Consumer: %s\n",message);
            buffer.remove(0);
        }
			
        cycle++;
	}	
}
...
```

9. En método `main(.)` se crearán dos `buffer`, uno para el consumidor y otro para el productor. Además se crea un objeto de la clase `Exchanger` que compartirán el **productor** y **consumidor** para el intercambio de los `buffer`. Se crean el productor y el consumidor y los hilos que los ejecutan. El código se puede revisar en el ejemplo de la sesión.

## Información adicional

Si el **productor** o el **consumidor** alcanzan el punto de intercambio antes que el otro hilo se encuentre preparado para realizar el intercambio se bloqueará hasta que ambos hilos se encuentren en el mismo punto.

## Ejercicios propuestos

- Revisar el [API de Java](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Exchanger.html) para ver todos los métodos de la clase `Exchanger` y comprender su utilidad.
- Modificar el [ejemplo 3](https://gitlab.com/ssccdd/guionsesion3/-/blob/master/Ejercicio3.md) de la sesión 3 para utilizar la clase `Exchanger` en la solución del problema del **Productor/Consumidor**.
<!--stackedit_data:
eyJoaXN0b3J5IjpbNTkyNjc4OTkxXX0=
-->